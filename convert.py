import re
import yaml
from datetime import datetime


def load_config(file_name: str) -> dict:
    with open(file_name, "r") as file:
        config: dict = yaml.safe_load(file)

    return config


class Task:

    def __init__(self, text: str, task_type: str) -> None:
        text_dict: dict = self._parse_task(text, task_type)
        self.task: str = text_dict.get("task")
        self.start_date: type[datetime | bool] = text_dict.get(
            "start_date", False)
        self.end_date: type[datetime | bool] = text_dict.get("end_date", False)
        self.priority: type[str | bool] = text_dict.get("priority", False)
        self.tags: dict = text_dict.get("tags", {})
        self.special_value: dict = text.get("special_value", {})

    def _parse_task(text: str, task_type: str) -> dict:

        def __completed_func():
            return {
                "task": r"",
                "start_date": r"",
                "end_date": r"",
                "priority": r"",
                "tags": r"",
                "context": r"",
                "special_value": r"",
            }

        def __ongoing_func():
            return {
                "task": r"",
                "start_date": r"",
                "end_date": r"",
                "priority": r"",
                "tags": r"",
                "context": r"",
                "special_value": r"",
            }

        __func_dict = {
            "completed": __completed_func,
            "ongoing": __ongoing_func,
        }


def retrieve_task(todo_file_path: str) -> dict:
    re_dict: dict = {
        "completed": re.compile(r"^x \d{4}-\d{2}-\d{2} .*\w", flags=re.M),
        "ongoing": re.compile(r"\(\w\).*\w|^[^x].*\w", flags=re.M),
    }
    task_dict: dict = {
        "completed": [],
        "ongoing": [],
    }

    with open(todo_file_path) as file:
        text: str = file.read()
        task_dict["completed"] = re_dict["completed"].findall(text)
        task_dict["ongoing"] = re_dict["ongoing"].findall(text)

    return task_dict
